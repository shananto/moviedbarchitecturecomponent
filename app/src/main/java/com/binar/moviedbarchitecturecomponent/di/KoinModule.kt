package com.binar.moviedbarchitecturecomponent.di

import com.binar.moviedbarchitecturecomponent.db.MovieDB
import com.binar.moviedbarchitecturecomponent.repository.MovieRepository
import com.binar.moviedbarchitecturecomponent.viewmodel.MovieViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val dbModule = module {
    single { MovieDB.getAppDataBase(context = get()) }
    factory { get<MovieDB>().movieDao() }
}
val repositoryModule = module {
    single { MovieRepository(get()) }
}
val uiModule = module {
    viewModel{ MovieViewModel(get()) }
}
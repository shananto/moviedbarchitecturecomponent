package com.binar.moviedbarchitecturecomponent

import android.app.Application
import com.binar.moviedbarchitecturecomponent.di.dbModule
import com.binar.moviedbarchitecturecomponent.di.repositoryModule
import com.binar.moviedbarchitecturecomponent.di.uiModule
import com.facebook.stetho.Stetho
import org.koin.android.ext.android.startKoin

class MyApp: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(
            dbModule,
            repositoryModule,
            uiModule
        ))
        Stetho.initializeWithDefaults(this)
    }
}
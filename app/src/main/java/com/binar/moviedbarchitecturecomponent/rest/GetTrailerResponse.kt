package com.binar.moviedbarchitecturecomponent.rest

import com.binar.moviedbarchitecturecomponent.db.entities.Trailer
import com.google.gson.annotations.SerializedName

data class GetTrailerResponse(
    @SerializedName("id")
    val id: Int,
    @SerializedName("results")
    val results: List<Trailer>
)
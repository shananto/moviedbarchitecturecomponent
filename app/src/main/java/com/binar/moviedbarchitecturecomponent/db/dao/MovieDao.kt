package com.binar.moviedbarchitecturecomponent.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.binar.moviedbarchitecturecomponent.db.entities.Movie

@Dao
interface MovieDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovie(movie: Movie)

    @Update
    fun updateMovie(movie: Movie)

    @Query("DELETE FROM tb_movies")
    fun deleteAllMovie()
//
//    @Query("SELECT * FROM tb_fav_movies WHERE title == :title")
//    fun getMovieByTitle(title: String): ArrayList<MovieNew>

    @Query("SELECT * FROM tb_movies")
    fun getAllMovies():LiveData<List<Movie>>
}
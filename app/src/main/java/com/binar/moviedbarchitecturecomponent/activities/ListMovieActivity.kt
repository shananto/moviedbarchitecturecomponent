package com.binar.moviedbarchitecturecomponent.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.binar.moviedbarchitecturecomponent.R
import com.binar.moviedbarchitecturecomponent.adapters.ListMovieAdapter
import com.binar.moviedbarchitecturecomponent.db.entities.Movie
import com.binar.moviedbarchitecturecomponent.viewmodel.MovieViewModel
import kotlinx.android.synthetic.main.activity_list_movie.*
import org.koin.android.ext.android.inject

class ListMovieActivity : AppCompatActivity() {
    private lateinit var adapter: ListMovieAdapter
    private val movieViewModel: MovieViewModel by inject()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_movie)
        adapter = ListMovieAdapter()
        adapter.notifyDataSetChanged()
        if (intent.extras != null) {
            val bundle = intent.extras
            val genreId = bundle?.getString("genreId")
            if (genreId != null) {
                movieViewModel.fetchMovieData(1, genreId, this)
            }
        }

        fetchData()
    }

    private fun fetchData() {
        movieViewModel.getMovieData().observe(this, Observer<List<Movie>> { items ->
            if (items != null) {
                setupRecyclerView(items)
            }
        })
    }

    private fun setupRecyclerView(items:List<Movie>) {
        adapter.setItem(items)
        adapter.setOnItemClickCallback(object : ListMovieAdapter.OnItemClickCallback{
            override fun onItemClicked(data: Movie) {
                selectMovie(data)
            }
        })

        rvMovie.layoutManager = GridLayoutManager(this,2)
        rvMovie.setHasFixedSize(true)
        rvMovie.isNestedScrollingEnabled = false
        rvMovie.adapter = adapter
    }

    private fun selectMovie(data: Movie) {
        Toast.makeText(this, data.getTitle(), Toast.LENGTH_SHORT).show()
        val intent = Intent(this, MovieDetailActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable("movie", data)
        intent.putExtras(bundle)
        startActivity(intent)
    }

}
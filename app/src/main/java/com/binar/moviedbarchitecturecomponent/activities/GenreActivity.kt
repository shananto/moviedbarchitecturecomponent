package com.binar.moviedbarchitecturecomponent.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.binar.moviedbarchitecturecomponent.R
import com.binar.moviedbarchitecturecomponent.adapters.GenreAdapter
import com.binar.moviedbarchitecturecomponent.db.entities.Genre
import com.binar.moviedbarchitecturecomponent.db.entities.Movie
import com.binar.moviedbarchitecturecomponent.viewmodel.MovieViewModel
import kotlinx.android.synthetic.main.activity_genre.*
import org.koin.android.ext.android.inject

class GenreActivity : AppCompatActivity() {
    var listBackdrop: ArrayList<String> = ArrayList()
    private val movieViewModel: MovieViewModel by inject()
    private lateinit var adapter: GenreAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_genre)
        adapter = GenreAdapter()
        adapter.notifyDataSetChanged()
        getTrending()
    }

    private fun getTrending() {
        movieViewModel.getTrending(this).observe(this, Observer<List<Movie>> { items ->
            if (items != null){
                for (item in items) {
                    item.getBackdropPath()?.let { it1 -> listBackdrop.add(it1)
                        Log.d("ContentValue", "fetchData: setbg $it1")}
                }
            }
            fetchData()
        })
    }

    private fun fetchData() {
        movieViewModel.getGenre(this).observe(this, Observer<List<Genre>> { items->
            if (items != null){
                if (listBackdrop.isNotEmpty()){
                    items.forEachIndexed { index, item ->
                        if (index < listBackdrop.size) {
                            item.setBackdropPath(listBackdrop[index])
                            Log.d("ContentValue", "fetchData: ${listBackdrop[index]}")
                        } else {
                            val size = listBackdrop.size
                            item.setBackdropPath(listBackdrop[index - size])
                        }
                    }
                }
                setupRecyclerView(items)
            }
        })
    }


    private fun setupRecyclerView(items: List<Genre>) {
        adapter.setItem(items)
        adapter.setOnItemClickCallback(object : GenreAdapter.OnItemClickCallback{
            override fun onItemClicked(data: Genre) {
                selectItem(data)
            }
        })


        rvGenre.layoutManager = LinearLayoutManager(this)
        rvGenre.setHasFixedSize(true)
        rvGenre.isNestedScrollingEnabled = false
        rvGenre.adapter = adapter
    }

    private fun selectItem(data: Genre) {
        Toast.makeText(this, data.getName(), Toast.LENGTH_SHORT).show()
        val intent = Intent(this, ListMovieActivity::class.java)
        intent.putExtra("genreId", data.getId().toString())
        startActivity(intent)
    }
}
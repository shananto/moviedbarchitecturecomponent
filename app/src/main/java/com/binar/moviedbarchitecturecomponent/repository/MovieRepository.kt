package com.binar.moviedbarchitecturecomponent.repository

import android.content.ContentValues.TAG
import android.content.Context
import android.os.AsyncTask
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.binar.moviedbarchitecturecomponent.db.dao.MovieDao
import com.binar.moviedbarchitecturecomponent.db.entities.Genre
import com.binar.moviedbarchitecturecomponent.db.entities.Movie
import com.binar.moviedbarchitecturecomponent.db.entities.Review
import com.binar.moviedbarchitecturecomponent.rest.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MovieRepository(private val movieDao: MovieDao) {
    private val api: API
    var listMovies = MutableLiveData<List<Movie>>()
    var listReview = MutableLiveData<List<Review>>()
    var listGenre = MutableLiveData<List<Genre>>()
    var listTrending = MutableLiveData<List<Movie>>()
    var key = MutableLiveData<String>()
    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        api = retrofit.create(API::class.java)
    }

    fun insert(item: Movie) {
        Log.d("State", "Inserting Data")
        InsertItemAsyncTask(
            movieDao
        ).execute(item)
    }

    private fun deleteAllMovies() {
        DeleteAllItemAsyncTask(
            movieDao
        ).execute()
    }

    fun getAllMovies(): LiveData<List<Movie>> {
        return movieDao.getAllMovies()
    }

    private class InsertItemAsyncTask(val movieDao: MovieDao) : AsyncTask<Movie, Unit, Unit>() {
        override fun doInBackground(vararg item: Movie?) {
            movieDao.insertMovie(item[0]!!)
        }
    }

    private class DeleteAllItemAsyncTask(val movieDao: MovieDao) : AsyncTask<Unit, Unit, Unit>() {
        override fun doInBackground(vararg p0: Unit?) {
            movieDao.deleteAllMovie()
        }
    }


    fun fetchMoviesData(page: Int, genre: String, context: Context?) {
        deleteAllMovies()
        api.setMovies(page = page, genre = genre)
            .enqueue(object : Callback<GetMoviesResponse> {
                override fun onResponse(
                    call: Call<GetMoviesResponse>,
                    response: Response<GetMoviesResponse>
                ) {
                    if (response.isSuccessful) {
                        val responseBody = response.body()

                        if (responseBody != null) {
                            listMovies.postValue(responseBody.movies)
                            val listData = responseBody.movies
                            listData?.let {
                                for (item in it)
                                {
                                    //inserting item to sqlite
                                    insert(item)
                                }
                            }
                        } else {
                            Toast.makeText(context, "error_fetch_movies", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(context, "error_fetch_movies", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<GetMoviesResponse>, t: Throwable) {
                    Toast.makeText(context, "error_fetch_movies", Toast.LENGTH_SHORT).show()
                }
            })
    }

    fun getMovieTrailer(movieId:Int, context: Context?) : LiveData<String> {

        api.getTrailer(movieId = movieId)
            .enqueue(object : Callback<GetTrailerResponse> {
                override fun onResponse(
                    call: Call<GetTrailerResponse>,
                    response: Response<GetTrailerResponse>
                ) {
                    if (response.isSuccessful) {
                        val responseBody = response.body()
                        if (responseBody != null) {
                            key.value = responseBody.results[0].getKey()!!
                        } else {
                            Toast.makeText(context, "error_fetching_data", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(context, "error_fetching_data", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<GetTrailerResponse>, t: Throwable) {
                    Toast.makeText(context, "error_fetching_data", Toast.LENGTH_SHORT).show()
                }
            })
        Log.d(TAG, "getMovieTrailer: $key")
        return key
    }

    fun getMovieReviews(movieId: Int, context: Context?) : LiveData<List<Review>> {

        api.getReview(movieId = movieId)
            .enqueue(object : Callback<GetReviewResponse> {
                override fun onResponse(
                    call: Call<GetReviewResponse>,
                    response: Response<GetReviewResponse>
                ) {
                    if (response.isSuccessful) {
                        val responseBody = response.body()
                        if (responseBody != null) {
                            listReview.postValue(responseBody.results)
                        } else {
                            Toast.makeText(context, "error_fetch_reviews", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(context, "error_fetch_reviews", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<GetReviewResponse>, t: Throwable) {
                    Toast.makeText(context, "error_fetch_reviews", Toast.LENGTH_SHORT).show()
                }
            })
        return listReview
    }

    fun getGenre(context: Context?): LiveData<List<Genre>>{
        api.getGenre()
            .enqueue(object : Callback<GetGenreResponse> {
                override fun onResponse(
                    call: Call<GetGenreResponse>,
                    response: Response<GetGenreResponse>
                ) {
                    if (response.isSuccessful) {
                        val responseBody = response.body()
                        if (responseBody != null) {
                            listGenre.postValue(responseBody.genre)
                        } else {
                            Toast.makeText(context, "error_fetch_genre", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(context, "error_fetch_genre", Toast.LENGTH_SHORT).show()
                    }
                }
                override fun onFailure(call: Call<GetGenreResponse>, t: Throwable) {
                    Toast.makeText(context, "error_fetch_genre", Toast.LENGTH_SHORT).show()
                }
            })
        return listGenre
    }
    fun getTrending(context: Context?): LiveData<List<Movie>> {
        api.getTrending()
            .enqueue(object : Callback<GetMoviesResponse> {
                override fun onResponse(
                    call: Call<GetMoviesResponse>,
                    response: Response<GetMoviesResponse>
                ) {
                    if (response.isSuccessful) {
                        val responseBody = response.body()

                        if (responseBody != null) {
                            listTrending.postValue(responseBody.movies)
                        } else {
                            Toast.makeText(context, "error_fetch_backdrop1", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(context, "error_fetch_backdrop2 ${response.code()}", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<GetMoviesResponse>, t: Throwable) {
                    Toast.makeText(context, "error_fetch_backdrop3", Toast.LENGTH_SHORT).show()
                }
            })
        return listTrending
    }
}
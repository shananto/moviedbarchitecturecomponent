package com.binar.moviedbarchitecturecomponent.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import com.binar.moviedbarchitecturecomponent.R
import com.binar.moviedbarchitecturecomponent.viewmodel.MovieViewModel
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import kotlinx.android.synthetic.main.fragment_youtube.*
import org.koin.android.ext.android.inject

class YoutubeFragment : DialogFragment() {

    private val movieViewModel: MovieViewModel by inject()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val p = dialog!!.window!!.attributes
        p.width = ViewGroup.LayoutParams.MATCH_PARENT
        p.dimAmount = 0f
        return inflater.inflate(R.layout.fragment_youtube, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycle.addObserver(youtube_player_view)
        if (arguments != null) {
            val movieId = arguments?.getInt("movieId")
            if (movieId != null) {
                movieViewModel.getMovieTrailer(movieId, activity)
                    .observe(viewLifecycleOwner, Observer { items ->
                        val videoKey = items
                        youtube_player_view.addYouTubePlayerListener(object :
                            AbstractYouTubePlayerListener() {
                            override fun onReady(youTubePlayer: YouTubePlayer) {
                                youTubePlayer.cueVideo(videoKey, 0f)
                            }
                        })
                    })
            }
        }
    }

    override fun onPause() {
        super.onPause()
        youtube_player_view.release()
    }
    override fun onDestroy() {
        super.onDestroy()
    }
}
package com.binar.moviedbarchitecturecomponent.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.binar.moviedbarchitecturecomponent.R
import com.binar.moviedbarchitecturecomponent.activities.MovieDetailActivity
import com.binar.moviedbarchitecturecomponent.adapters.ReviewAdapter
import com.binar.moviedbarchitecturecomponent.db.entities.Movie
import com.binar.moviedbarchitecturecomponent.db.entities.Review
import com.binar.moviedbarchitecturecomponent.viewmodel.MovieViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import kotlinx.android.synthetic.main.fragment_detail_movie.*
import org.koin.android.ext.android.inject


class DetailMovieFragment : Fragment() {
    private val movieViewModel: MovieViewModel by inject()
    private lateinit var adapter: ReviewAdapter
    private lateinit var currentMovie: Movie
    private lateinit var youTubePlayerView: YouTubePlayerView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_movie, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = ReviewAdapter()
        adapter.notifyDataSetChanged()
        onClickGroup()
        setupView()
        fetchData()
    }

    private fun setupView() {
        if(arguments != null)
        {
            //get selected movie
            currentMovie = arguments!!.getSerializable("selectedMovie") as Movie
            currentMovie.getPosterPath().let { posterPath ->
                Glide.with(this)
                    .load("https://image.tmdb.org/t/p/w342$posterPath")
                    .transform(CenterCrop())
                    .into(ivPoster)
            }
            currentMovie.getBackdropPath().let { posterPath ->
                Glide.with(this)
                    .load("https://image.tmdb.org/t/p/w342$posterPath")
                    .transform(CenterCrop())
                    .into(ivBackdrop)
            }
            tvDetailMovieTitle.text = currentMovie.getTitle()
            tvDetailMovieYear.text = currentMovie.getReleaseDate()
            tvMovieDetailRate.text = currentMovie.getVoteAverage().toString()
            tvDetailDescription.text = currentMovie.getOverview()
        }
    }

    private fun onClickGroup() {
        btnSeeAllReview.setOnClickListener{
            (activity as MovieDetailActivity).showAllReview(currentMovie.getId())
        }
        btnPlayTrailer.setOnClickListener {
            showTrailer()
        }
    }

    private fun showTrailer(){
        val youTubeFragment = YoutubeFragment()
        val bundle = Bundle()
        currentMovie.getId()?.let { bundle.putInt("movieId", it) }
        youTubeFragment.arguments = bundle
        var ft: FragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        ft.addToBackStack(null)
        youTubeFragment.show(ft, "dialog")
    }
    private fun fetchData() {
        movieViewModel.getMovieReviews(currentMovie.getId()!!, context).observe(viewLifecycleOwner, Observer<List<Review>> { items->
            if (items != null){
                setupRecyclerView(items)
            }
        })
    }

    private fun setupRecyclerView(item: List<Review>) {
        val selectedItem = item.take(1)
        adapter.setItem(selectedItem)
        adapter.setOnItemClickCallback(object : ReviewAdapter.OnItemClickCallback{
            override fun onItemClicked(data: Review) {
                selectReview(data)
            }
        })

        rvDetailReview.layoutManager = LinearLayoutManager(activity)
        rvDetailReview.setHasFixedSize(true)
        rvDetailReview.isNestedScrollingEnabled = false
        rvDetailReview.adapter = adapter
    }

    private fun selectReview(data: Review) {
        Toast.makeText(activity, data.getAuthor(), Toast.LENGTH_SHORT).show()
    }


    override fun onDestroy() {
        super.onDestroy()
    }
}